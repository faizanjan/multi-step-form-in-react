import React, { useState } from "react";
import Aside from "./Aside";

let Form = ({ children, validity }) => {
  let [currentPage, setCurrentPage] = useState(0);

  const nextPage = (e) => {
    e.preventDefault();
    if (currentPage === 0 && !validity) return;
    setCurrentPage(currentPage + 1);
  };

  const previousPage = () => {
    setCurrentPage(currentPage - 1);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setCurrentPage(currentPage + 1);
    // Handle form submission
  };

  return (
    <form className="App my-container d-flex flex-column flex-lg-row bg-light border border-light rounded-3 h-75 p-lg-3">
      <Aside step={currentPage} />

      {React.Children.toArray(children)[currentPage]}

      {currentPage > 0 && currentPage < 4 && (
        <button
          className="btn btn-rounded btn-outline-dark prev-btn p-2 px-4"
          onClick={previousPage}
        >
          Previous
        </button>
      )}
      {currentPage < 3 ? (
        <button
          onClick={(e)=>{nextPage(e)}}
          className="btn btn-rounded btn-dark next-btn p-2 px-4"
        >
          Next
        </button>
      ) : currentPage < 4 && (
        <button
          onClick={(e)=>{handleSubmit(e)}}
          className="btn btn-rounded btn-dark next-btn p-2 px-4"
        >
          Submit
        </button>
      )}
    </form>
  );
};

export default Form;
