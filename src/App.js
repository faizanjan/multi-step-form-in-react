import Form from "./components/Form";
import Step1 from "./components/Step1";
import Step2 from "./components/Step2";
import Step3 from "./components/Step3";
import Step4 from "./components/Step4";
import Step5 from "./components/Step5";

import plans from "./data/plans.js";
import addOns from "./data/addOns.js";

import validateFirstStep from "./modules/validation";

import { useState, useEffect } from "react";

function App() {
  // STATE MANAGEMENT
  let [personalInfo, setPersonalInfo] = useState(
    JSON.parse(sessionStorage.getItem("personalInfo"))
  );

  let lastMonthly = JSON.parse(sessionStorage.getItem("isMonthly"));
  let [isMonthly, switchMonthly] = useState(
    lastMonthly === null ? true : lastMonthly
  );

  let lastActivePlan =
    JSON.parse(sessionStorage.getItem("activePlan")) || plans[0];
  let [activePlan, setActivePlan] = useState(lastActivePlan);

  let lastAddOnsList =
    JSON.parse(sessionStorage.getItem("addOnsList")) || addOns;
  let [addOnsList, setAddOnsList] = useState(lastAddOnsList);

  let [validity, setValidity] = useState(validateFirstStep(personalInfo));

  // COMPONENT DID UPDATE
  useEffect(() => {
    sessionStorage.setItem("personalInfo", JSON.stringify(personalInfo));
    sessionStorage.setItem("isMonthly", JSON.stringify(isMonthly));
    sessionStorage.setItem("activePlan", JSON.stringify(activePlan));
    sessionStorage.setItem("addOnsList", JSON.stringify(addOnsList));
    setValidity(validateFirstStep(personalInfo));
  }, [isMonthly, activePlan, addOnsList, personalInfo]);

  let handlePersonalInfo = (key, value) => {
    setPersonalInfo({
      ...personalInfo,
      [key]: value,
    });
  };

  let setPlan = (index) => {
    setActivePlan(plans[index]);
  };

  let modifyAddOns = (id, checked) => {
    let newList = addOnsList.map((addOn) => {
      if (addOn.id === id) {
        return {
          ...addOn,
          checked,
        };
      } else return addOn;
    });
    setAddOnsList(newList);
  };

  // JSX
  return (
    <Form validity={validity}>
      <Step1
        personalInfo={personalInfo}
        handlePersonalInfo={handlePersonalInfo}
        prompts={validity[1]}
      />
      <Step2
        isMonthly={isMonthly}
        switchMonthly={switchMonthly}
        setPlan={setPlan}
        activePlan={activePlan}
      />
      <Step3
        isMonthly={isMonthly}
        modifyAddOns={modifyAddOns}
        addOnsList={addOnsList}
      />
      <Step4
        isMonthly={isMonthly}
        activePlan={activePlan}
        addOnsList={addOnsList}
      />
      <Step5 />
    </Form>
  );
}

export default App;
